//
// Created by Greg Dutcher on 9/2/18.
//

#include "./lib/googletest/googletest/include/gtest/gtest.h"
#include "../src/Tokenizer.h"

#include <queue>
#include <gtest/gtest.h>

TEST( tokenizerTest, testConvertsStringsWithOnlyLettersToLettersGroupToken ) {
    Tokenizer tokenizer;
    std::queue<Token*> tokens = tokenizer.read("abc");

    EXPECT_EQ( tokens.size(), 1 );
    EXPECT_EQ(tokens.front()->getCharacters(), "abc" );
}

TEST( tokenizerTest, setsAreParsedAsOpenBracesFollowedByCommaSeparatedItems ) {
    Tokenizer tokenizer;
    std::queue<Token*> tokens = tokenizer.read("{ab,c}");

    EXPECT_EQ( tokens.size(), 5 );
    EXPECT_EQ(tokens.front()->getCharacters(), "{" );
    tokens.pop();
    EXPECT_EQ(tokens.front()->getCharacters(), "ab" );
    tokens.pop();
    EXPECT_EQ(tokens.front()->getCharacters(), "," );
    tokens.pop();
    EXPECT_EQ(tokens.front()->getCharacters(), "c" );
    tokens.pop();
    EXPECT_EQ(tokens.front()->getCharacters(), "}" );
}

//TEST_CASE( "expressions with adjacent delimiter tokens are parsed as having an empty letters group between them" ) {
//    Tokenizer tokenizer;
//    std::vector tokens = tokenizer.read("{a,,c,}");
//
//    REQUIRE( tokens.size() == 5 );
//    REQUIRE(tokens[0]->getCharacters() == "{" );
//    REQUIRE(tokens[1]->getCharacters() == "a" );
//    REQUIRE(tokens[2]->getCharacters() == "," );
//    REQUIRE(tokens[3]->getCharacters() == "" );
//    REQUIRE(tokens[4]->getCharacters() == "," );
//    REQUIRE(tokens[5]->getCharacters() == "c" );
//    REQUIRE(tokens[4]->getCharacters() == "," );
//    REQUIRE(tokens[3]->getCharacters() == "" );
//    REQUIRE(tokens[6]->getCharacters() == "}" );
//}
