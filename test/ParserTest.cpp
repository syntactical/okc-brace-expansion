//
// Created by Greg Dutcher on 9/3/18.
//

#define CATCH_CONFIG_MAIN
#include <queue>
#include <gtest/gtest.h>
#include "../src/Parser.h"
#include "../src/tokens/StartExpressionToken.h"
#include "../src/tokens/LettersGroupToken.h"
#include "../src/tokens/ExpressionDelimiterToken.h"
#include "../src/tokens/EndExpressionToken.h"

TEST( parserTest, stringsAreReturnedAsIs ){
    Parser parser;

    LettersGroupToken t("abcdef");

    std::queue<Token*> tokens;
    tokens.push(&t);

    std::vector<std::string> items = parser.parse(tokens);

    EXPECT_EQ( items.size(), 1 );
    EXPECT_EQ(items[0], "abcdef" );
}

TEST( parserTest, expressionsInBracesAreExpanded ) {
    Parser parser;
    std::queue<Token *> tokens;

    tokens.push(new LettersGroupToken("a"));
    tokens.push(new StartExpressionToken());
    tokens.push(new LettersGroupToken("1"));
    tokens.push(new ExpressionDelimiterToken());
    tokens.push(new LettersGroupToken("2"));
    tokens.push(new ExpressionDelimiterToken());
    tokens.push(new LettersGroupToken("3"));
    tokens.push(new EndExpressionToken());
    tokens.push(new LettersGroupToken("b"));

    std::vector<std::string> items = parser.parse(tokens);

    EXPECT_EQ( items.size(), 3 );
    EXPECT_EQ(items[0], "a1b" );
    EXPECT_EQ(items[1], "a2b" );
    EXPECT_EQ(items[2], "a3b" );
}
