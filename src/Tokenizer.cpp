//
// Created by Greg Dutcher on 9/2/18.
//

#include <queue>

#include "Tokenizer.h"
#include "tokens/LettersGroupToken.h"
#include "tokens/StartExpressionToken.h"
#include "tokens/EndExpressionToken.h"
#include "tokens/ExpressionDelimiterToken.h"

queue<Token*> Tokenizer::read(std::string expression) {
    queue<Token*> tokens;

    std::string::iterator it = expression.begin();

    while(it != expression.end()) {
        if (*it == '{'){
            tokens.push(new StartExpressionToken());
        } else if (*it == ',') {
            tokens.push(new ExpressionDelimiterToken());
        } else if (isalnum(*it)) {
            std::string lettersGroup;
            while (it != expression.end() && isalnum(*it)) {
                lettersGroup += *it;
                it++;
            }
            it--;
            tokens.push(new LettersGroupToken(lettersGroup));
        } else if (*it == '}') {
            tokens.push(new EndExpressionToken());
        }
        it++;
    }

    return tokens;
}
