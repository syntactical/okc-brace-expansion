//
// Created by Greg Dutcher on 9/3/18.
//

#ifndef OKC_PARSER_H
#define OKC_PARSER_H


#include <string>
#include <queue>
#include "tokens/Token.h"

class Parser {
private:
    std::vector<std::string> parseSet(std::queue<Token*>& tokens);
    std::vector<std::string> parseExpression(std::queue<Token*>& tokens);
    std::vector<std::string> parseTerm(std::queue<Token*>& tokens);
    std::vector<std::string> expand(std::vector<std::string> leftItems, std::vector<std::string> rightItems);
public:
    std::vector<std::string> parse(std::queue<Token*> tokens);
};


#endif //OKC_PARSER_H
