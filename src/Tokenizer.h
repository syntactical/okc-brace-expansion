//
// Created by Greg Dutcher on 9/2/18.
//

#ifndef OKC_TOKENIZER_H
#define OKC_TOKENIZER_H


#include <queue>
#include <string>
#include "tokens/Token.h"

class Tokenizer {
private:
    std::queue<Token*> tokens;
public:
    std::queue<Token*> read(std::string expression);
};

#endif //OKC_TOKENIZER_H
