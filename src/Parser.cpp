//
// Created by Greg Dutcher on 9/3/18.
//

#include <stdexcept>
#include <vector>
#include <iostream>

#include "Parser.h"
#include "tokens/StartExpressionToken.h"
#include "tokens/LettersGroupToken.h"

std::vector<std::string> Parser::parse(std::queue<Token *> tokens) {
    std::vector<std::string> items;

    items = Parser::parseSet(tokens);

    return items;
}

std::vector<std::string> Parser::parseSet(std::queue<Token *>& tokens) {
    std::vector<std::string> items;

    while (!tokens.empty()){
        std::cout << "from parse set: " << tokens.front()->getCharacters() << "\n\n\n\n";
        std::vector<std::string> itemsFromExpression = Parser::parseExpression(tokens);
        items.insert(items.end(), itemsFromExpression.begin(), itemsFromExpression.end());

        if (!tokens.empty() && tokens.front()->getCharacters()== ","){
            tokens.pop();
        }
    }

    std::cout << "returning items: " << items.size() << "\n\n\n\n";

    return items;
}

std::vector<std::string> Parser::parseExpression(std::queue<Token *>& tokens) {
    std::vector<std::string> items;
    if (tokens.front()->getCharacters() == "{") {
        tokens.pop();
    }

    while (!tokens.empty() && tokens.front()->getCharacters() != "}") {
        std::vector<std::string> itemsInNextTerm = Parser::parseTerm(tokens);

        std::cout << "size of items from parseTerm: " << itemsInNextTerm.size() << "\n\n\n\n";

        if (itemsInNextTerm.empty()) {
            break;
        }

        if (items.empty()) {
            items = itemsInNextTerm;
        } else {
            items = Parser::expand(items, itemsInNextTerm);
        }
    }

    if (!tokens.empty() && tokens.front()->getCharacters() == "}") {
        tokens.pop();
    }


    return items;
}

std::vector<std::string> Parser::parseTerm(std::queue<Token *>& tokens) {
    std::vector<std::string> items;
    std::cout << "from parse term: " << tokens.front()->getCharacters() << "\n\n\n\n";
    if (tokens.front()->getCharacters() == "{") {
        items = Parser::parseSet(tokens);
    } else if (isalnum(tokens.front()->getCharacters()[0])) {
        std::cout << "from parse term inside alnum: " << tokens.front()->getCharacters() << "\n\n\n\n";
        items.push_back(tokens.front()->getCharacters());
        tokens.pop();
    } else {
        std::cout << "bad input apparently: " << tokens.front()->getCharacters() << "\n\n\n\n";
        throw std::invalid_argument("bad input");
    }

    return items;
}

std::vector<std::string> Parser::expand(std::vector<std::string> leftItems, std::vector<std::string> rightItems) {
    std::vector<std::string> items;

    for (auto &leftItem : leftItems) {
        for (auto &rightItem : rightItems) {
            items.push_back(leftItem + rightItem);
        }
    }

    return items;
}


