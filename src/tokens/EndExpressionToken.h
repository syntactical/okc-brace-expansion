//
// Created by Greg Dutcher on 9/3/18.
//

#include "Token.h"

#ifndef OKC_CLOSEBRACETOKEN_H
#define OKC_CLOSEBRACETOKEN_H

#endif //OKC_CLOSEBRACETOKEN_H

class EndExpressionToken: public Token {
public:
    std::string getCharacters() override;
    const char DESIGNATED_CHAR = '}';
};

