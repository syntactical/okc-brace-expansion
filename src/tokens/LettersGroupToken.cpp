#include <utility>

//
// Created by Greg Dutcher on 9/2/18.
//

#include "LettersGroupToken.h"

LettersGroupToken::LettersGroupToken(string _letters) {
    letters = _letters;
}

string LettersGroupToken::getCharacters() {
    return letters;
}
