//
// Created by Greg Dutcher on 9/2/18.
//

#ifndef OKC_TOKEN_H
#define OKC_TOKEN_H

#include <string>

class Token
{
private:
    std::string characters;
public:
    virtual std::string getCharacters() = 0;
};

#endif //OKC_TOKEN_H
