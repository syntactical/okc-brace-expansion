//
// Created by Greg Dutcher on 9/2/18.
//

#ifndef OKC_LETTERSGROUPTOKEN_H
#define OKC_LETTERSGROUPTOKEN_H

#include <string>

#include "Token.h"

using namespace std;

class LettersGroupToken: public Token {
private:
    string letters;
public:
    explicit LettersGroupToken(string _letters);
    string getCharacters() override;
};

#endif //OKC_LETTERSGROUPTOKEN_H
