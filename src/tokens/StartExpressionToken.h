//
// Created by Greg Dutcher on 9/3/18.
//

#ifndef OKC_OPENBRACETOKEN_H
#define OKC_OPENBRACETOKEN_H

#include "Token.h"

class StartExpressionToken: public Token {
public:
    std::string getCharacters() override;
    const char DESIGNATED_CHAR = '{';
};

#endif //OKC_OPENBRACETOKEN_H
