//
// Created by Greg Dutcher on 9/3/18.
//

#include "Token.h"

#ifndef OKC_SEPARATORTOKEN_H
#define OKC_SEPARATORTOKEN_H

class ExpressionDelimiterToken: public Token {
public:
    std::string getCharacters() override;
    const char DESIGNATED_CHAR = ',';
};
#endif //OKC_SEPARATORTOKEN_H
